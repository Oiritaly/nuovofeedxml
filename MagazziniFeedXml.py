import json
from pandas.io.json import json_normalize
import ijson
import pandas as pd
from pprint import pprint
import collections
from xml.dom.minidom import parseString
import requests
from dicttoxml import dicttoxml
from itertools import groupby

#Questa versione del feed magazzini no distingue le varianti prodotto
#Gestisce solo i prodotti che sono presenti su uno o piu magazzini

# Connessione API e raccolta dati
api_url = "https://admin.oiritaly.it/api"
headers = {'Authorization': 'api-key 8ff5b7baa10cdfbaf4d973b674363b6ef83561d5'}

listino = {"query": '''
{stocks(first: 100) {
edges {
node {
sku
quantity
quantityAllocated
location {
... Location
}
}
}
pageInfo {
    hasNextPage
    endCursor
    }
}
}
fragment Location on StockLocation {
name
isActive
onVacation
}
'''}


results_prod = requests.post(url=api_url, json=listino)
pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
data = json.loads(pjson)
dp = pd.DataFrame.from_dict(json_normalize(data['data']['stocks']))
df = pd.DataFrame.from_dict(json_normalize(data['data']['stocks']['edges']))
pg = pd.DataFrame.from_dict(json_normalize(data['data']['stocks']['pageInfo']))
NextPage = pg['hasNextPage'][0]
cursor = pg['endCursor'][0]

while NextPage:
    listino = {"query": '''
    {stocks(first: 100, after:"''' + cursor + '''") 
    {
    edges {
    node {
    sku
    quantity
    quantityAllocated
    location {
    ... Location
    }
    }
    }
    pageInfo {
    hasNextPage
    endCursor
    }
    }
    }
    fragment Location on StockLocation {
    name
    isActive
    onVacation
    }
    '''}
    results_prod = requests.post(url=api_url, json=listino)
    pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
    data = json.loads(pjson)
    pdf = pd.DataFrame.from_dict(json_normalize(data['data']['stocks']['edges']))
    fpg = pd.DataFrame.from_dict(json_normalize(data['data']['stocks']['pageInfo']))
    NextPage = fpg['hasNextPage'][0]
    cursor = fpg['endCursor'][0]
    df = df.append(pdf, ignore_index=True, sort=None)
#Unione listini per codice prodotto



#Formattazione campi BindCommerce
dfn = pd.DataFrame()
dcode = pd.DataFrame()


# elimina righe per magazzini non attivi
df = df.drop(df[df['node.location.onVacation'].map(lambda x: x is True)].index)
# elimina righe per magazzini in ferie
df = df.drop(['node.location.isActive', 'node.location.onVacation'],axis=1)
# elimina righe per magazzini con Qty=0
df = df.drop(df[df['node.quantity'].map(lambda x: x <= 0)].index)

df['Qty'] = df['node.quantity']-df['node.quantityAllocated']
df = df.drop(['node.quantity','node.quantityAllocated'], axis=1)
df = df.rename(columns={'node.location.name': 'WarehouseKey'})
df.set_index('node.sku')
dfn['Warehouse'] = df[['WarehouseKey', 'Qty']].to_dict(orient='record', into=dict)
dfn['Code'] = df['node.sku']


# Ordinamento per Codice e per Vendor
dfn = dfn.sort_values(by=['Code'])
#Ricerca codici duplicati e raggruppa i magazzini per codice prodotto
mask = dfn.Code.duplicated(keep=False)
dfn['Duplicati'] = (dfn.Code.mask(~mask, 0))
dp = dfn[dfn['Duplicati'] > 0]
dp = dp.groupby(['Duplicati'])['Warehouse'].agg({'Warehouses': lambda x: x.to_list()})
dp = dp.reset_index(['Duplicati'])
dp = dp.rename(columns={'Duplicati': 'Code'})
#Prepara magazzini di prodotti senza duplicati
dfn = dfn.drop(dfn[dfn['Duplicati'].map(lambda x: x != 0)].index)
dfn = dfn.drop(['Duplicati'], axis=1)
dfn['Warehouses'] = dfn[['Warehouse']].to_dict(orient='record', into=dict)
dfn = dfn.drop(['Warehouse'], axis=1)
#Unione tra magazzini di prodotti singoli e magazzini di duplicati
dfn=dfn.append(dp, ignore_index=True)

#Convesione nuovo file json e serializzazione

dfj = dfn.to_json(None, orient='records')
ppjson = json.loads(dfj,object_pairs_hook=collections.OrderedDict)

# creazione file xml

prod = lambda x: x[:-1]
xml = dicttoxml(ppjson, custom_root='Products', attr_type=False, item_func=prod)
dom = parseString(xml)
print(dom.toprettyxml())
output = open("magazzini.xml", "w")
output.write(dom.toprettyxml(encoding="utf-8"))

