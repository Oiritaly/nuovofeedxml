import json
from pandas.io.json import json_normalize
import ijson
import pandas as pd
from pprint import pprint
from xml.dom.minidom import parseString
import requests
from dicttoxml import dicttoxml, set_debug
import collections
import datetime
import collections
import unicodedata as ud
# Connessione API e raccolta dati
api_url = "https://admin.oiritaly.it/api"
headers = {'Authorization': 'api-key 8ff5b7baa10cdfbaf4d973b674363b6ef83561d5'}
products = {"query": '''
{products (first:100) 
{edges 
{node 
{sku 
ean 
name 
productType 
category 
brand 
attributes
{name 
value 
}
descriptions {
    language
    description
  }
}}
pageInfo {
hasNextPage
endCursor
}
}
}
'''}

results_prod = requests.post(url=api_url, json=products)
pjsonp = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
datap = json.loads(pjsonp)
# with open('prodotti.json') as data_file:    data = json.loads(data_file.read())
dfp = pd.DataFrame.from_dict(json_normalize(datap['data']['products']['edges']))
pgp = pd.DataFrame.from_dict(json_normalize(datap['data']['products']['pageInfo']))
# at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))

NextPage = pgp['hasNextPage'][0]
cursor = pgp['endCursor'][0]

while NextPage:
    products = {"query": '''
    {products (first:100, after:"''' + cursor + '''") 
    {edges 
    {node 
    {sku 
    ean 
    name 
    productType 
    category 
    brand 
    attributes
    {name 
    value 
    }
    descriptions {
        language
        description
      }
    }}
    pageInfo {
    hasNextPage
    endCursor
    }
    }
    }
    '''}
    results_prod = requests.post(url=api_url, json=products)
    pjsonp = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
    datap = json.loads(pjsonp)
    #with open('prodotti.json') as data_file:    data = json.loads(data_file.read())
    pdfp = pd.DataFrame.from_dict(json_normalize(datap['data']['products']['edges']))
    fpgp = pd.DataFrame.from_dict(json_normalize(datap['data']['products']['pageInfo']))
    #at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))
    #concatenare dataframe pandas
    NextPage = fpgp['hasNextPage'][0]
    cursor = fpgp['endCursor'][0]
    dfp = dfp.append(pdfp, ignore_index=True, sort=None)

language = 'Italian'
dfnp = pd.DataFrame()
dfnp['Code'] = dfp['node.sku']
dfnp['Barcode'] = dfp['node.ean']
dfnp['BarcodeKind'] = 'EAN'
dfnp['Title'] = dfp['node.name']
#for i in df['node.descriptions'].iteritems():
dfnp['ShortDescription'] = ''      #da comporre con lista di attributi da definire con Cristiano
dfnp['Categories'] = dfp['node.productType']
#Le descrizioni in lingua sono disattivate
#dfn['Alternatives']=df['node.descriptions']
#for key, value in df['node.descriptions'].iteritems():
  #  for i in range(len(df['node.descriptions'][key])):
       ## #if df['node.attributes'][key][i]['name'] is set:
        #    dfn['Alternatives'][key][i]['Language'] = df['node.descriptions'][key][i]['language']
         #   dfn['Alternatives'][key][i].pop('language')
        #  dfn['Alternatives'][key][i]['DescriptionHtml'] = df['node.descriptions'][key][i]['description']
         #   dfn['Alternatives'][key][i].pop('description')

dfnp['Manufacturer'] = dfp['node.brand']
dfnp['Qty'] = ''
dfnp['Pictures'] = ''
dfnp['Attributes'] = dfp['node.attributes']
for key, value in dfp['node.attributes'].iteritems():
    for i, v in enumerate(dfp['node.attributes'][key]):
        if dfp['node.attributes'][key][i]['value'] != ' ' and dfp['node.attributes'][key][i]['value'] != '' and dfp['node.attributes'][key][i]['value'] != '0' and dfp['node.attributes'][key][i]['value'] != '- non applicabile':
            dfnp['Attributes'][key][i]['Name'] = dfnp['Attributes'][key][i]['name']
            dfnp['Attributes'][key][i]['Value'] = dfnp['Attributes'][key][i]['value']
        dfnp['Attributes'][key][i].pop('name')
        dfnp['Attributes'][key][i].pop('value')
        dfnp['Attributes'][key][i] = collections.OrderedDict(sorted(dfnp['Attributes'][key][i].items())) #Ordina gli item Name e Value del dizionario

#Eliminazione degli attributi vuoti
for key, value in dfnp['Attributes'].iteritems():
    while {} in value:
        value.remove({})

def shortdescription(cat,attribList):#Funzione per produrre la descrizione breve
    for key, value in dfnp['Attributes'].iteritems():
        for k, v in enumerate(value):
            if dfnp['Categories'][key] == cat and dfnp['Attributes'][key][k]['Name'] in attribList:  # Definire quali attributi inviare nella descrizione
                dfnp['ShortDescription'][key] = dfnp['ShortDescription'][key] + dfnp['Attributes'][key][k]['Value'] + ' '

#Creo descrizione breve con il Titolo
for key, value in dfnp['Attributes'].iteritems():
    for k, v in enumerate(value):
        if dfnp['Attributes'][key][k]['Name'] == 'Titolo': #Definire quali attributi inviare nella descrizione
            dfnp['ShortDescription'][key] = dfnp['ShortDescription'][key] + dfnp['Attributes'][key][k]['Value'] + ' '
#Creo descrizione breve ottica con lista attributi
AttribDesc = ['Genere utente', 'Forma occhiale', 'Materiale occhiali', 'Colore montatura']
shortdescription('Occhiali e montature', AttribDesc)
shortdescription('Lenti a contatto', AttribDesc)
#Creo descrizione breve gioielli con lista attributi
AttribDescGioi=['Genere', 'Materiale', 'Colore']
shortdescription('Gioielli', AttribDescGioi)
#Creo descrizione breve orologi con lista attributi
AttribDescWatch=['Genere', 'Alimentazione movimento', 'Materiale', 'Display', 'Colore']
shortdescription('Orologi', AttribDescWatch)

# Query sui magazzini
magazzino = {"query": '''
{stocks(first: 100) {
edges {
node {
sku
quantity
quantityAllocated
location {
... Location
}
}
}
pageInfo {
    hasNextPage
    endCursor
    }
}
}
fragment Location on StockLocation {
name
vendor
isActive
onVacation
}
'''}


results_prod = requests.post(url=api_url, json=magazzino)
pjsons = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
datas = json.loads(pjsons)
dps = pd.DataFrame.from_dict(json_normalize(datas['data']['stocks']))
dst = pd.DataFrame.from_dict(json_normalize(datas['data']['stocks']['edges']))
pgs = pd.DataFrame.from_dict(json_normalize(datas['data']['stocks']['pageInfo']))
NextPage = pgs['hasNextPage'][0]
cursor = pgs['endCursor'][0]

while NextPage:
    magazzino = {"query": '''
    {stocks(first: 100, after:"''' + cursor + '''") 
    {
    edges {
    node {
    sku
    quantity
    quantityAllocated
    location {
    ... Location
    }
    }
    }
    pageInfo {
    hasNextPage
    endCursor
    }
    }
    }
    fragment Location on StockLocation {
    name
    vendor
    isActive
    onVacation
    }
    '''}
    results_prod = requests.post(url=api_url, json=magazzino)
    pjsons = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
    datas = json.loads(pjsons)
    pdfs = pd.DataFrame.from_dict(json_normalize(datas['data']['stocks']['edges']))
    fpgs = pd.DataFrame.from_dict(json_normalize(datas['data']['stocks']['pageInfo']))
    NextPage = fpgs['hasNextPage'][0]
    cursor = fpgs['endCursor'][0]
    dst = dst.append(pdfs, ignore_index=True, sort=None)

#Formattazione campi BindCommerce
dfs = pd.DataFrame()
dcode = pd.DataFrame()

# elimina righe per magazzini non attivi
dst = dst.drop(dst[dst['node.location.isActive'].map(lambda x: x is False)].index)
# elimina righe per magazzini in ferie
dst = dst.drop(['node.location.isActive', 'node.location.onVacation'], axis=1)
#Preparazione pd con i prodotti-listini da filtrare relativi a disponibilita nulle
dcode['Code'] = dst['node.sku']
dcode['VendorCode'] = dst['node.location.vendor']# Codice del vendor relativo a quel magazzino
dcode['Qty'] = dst['node.quantity']
dcode = dcode.drop(dcode[dcode['Qty'].map(lambda x: x > 0)].index)# Salvo i magazzini con Qty nulla
# elimina righe per magazzini con Qty=0
dst = dst.drop(dst[dst['node.quantity'] <= 0].index)
dst['Qty'] = dst['node.quantity'] - dst['node.quantityAllocated']
dst = dst.rename(columns={'node.sku': 'Code'})

#Calcolo le quantita totali dei prodotti partendo dai magazzini
# Assegnazione della quantita dai magazzini al campo Qty dei prodotti
for key, value in dfnp['Code'].iteritems():
    for k, v in dst['Code'].iteritems():
        if v == value:
            if dfnp['Qty'][key] != '' and dfnp['Qty'][key] > 0:
                dfnp['Qty'][key] += dst['Qty'][k]
            else:
                dfnp['Qty'][key] = dst['Qty'][k]


#Assegna Qty=0 anche a prodotti senza magazzino per poi eliminare le righe
dfnp['Qty'] = dfnp['Qty'].replace('', 0)
# Elimina dai prodotti le righe con quantita totale nulla
dfnp = dfnp.drop(dfnp[dfnp['Qty'] <= 0].index)
dst = dst.drop(['node.quantity', 'node.quantityAllocated'], axis=1) # Elimina colonne inutili
#Preparazione del dizionario Warehouse
dst = dst.rename(columns={'node.location.name': 'WarehouseKey'})
dst.set_index('Code')
dst['Warehouse'] = dst[['WarehouseKey', 'Qty']].to_dict(orient='record', into=dict)
dst.reset_index()
dst = dst.drop(['node.location.vendor', 'Qty', 'WarehouseKey'], axis=1)

# Ordinamento per Codice e per Vendor
dfs = dst
dfs = dfs.sort_values(by=['Code'])
#Ricerca codici duplicati e raggruppa i magazzini per codice prodotto
mask = dfs.Code.duplicated(keep=False)
dfs['Duplicati'] = (dfs.Code.mask(~mask, 0))
dps = dfs[dfs['Duplicati'] > 0]
dps = dps.groupby(['Duplicati'])['Warehouse'].agg({'Warehouses': lambda x: x.to_list()})
dps = dps.reset_index(['Duplicati'])
dps = dps.rename(columns={'Duplicati': 'Code'})
#Prepara magazzini di prodotti senza duplicati
dfs = dfs.drop(dfs[dfs['Duplicati'].map(lambda x: x != 0)].index)
dfs = dfs.drop(['Duplicati'], axis=1)
dfs['Warehouses'] = dfs[['Warehouse']].to_dict(orient='record', into=dict)
dfs = dfs.drop(['Warehouse'], axis=1)
#Unione tra magazzini di prodotti singoli e magazzini di duplicati
dfs = dfs.append(dps, ignore_index=True)
#dfs.to_csv("stockFull.csv", ',', encoding='utf-8')


# Query Lists per i listini dei prezzi -----------------------------------------------------
list = {"query": '''
{ lists(first: 100)
{edges {
      node {
       ...List
} } 
pageInfo {
hasNextPage
endCursor
}
}} 
fragment List on List {
  sku 
  vendorCode
  availableMeasure
  customMeasurePrice {
    ...Price
  }
  customAvailableMeasuresFrom
  customAvailableMeasuresTo
  listPrice {
    ...Price
  }
  viewOnlyListPrice
  isListActive
  groups {
    ...Group
  }
}

fragment Price on Money {
  amount
}

fragment Group on ChannelList {
  group
  isChannelActive
  price {
    ...Price
  }
  priceInclTax {
    ...Price
  }
  discount {
    ...Price
  }
  discountPercentage
  discountFromDate
  discountToDate
  temporaryDiscountPercentage
}
'''}

results_prod = requests.post(url=api_url, json=list)
pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
data = json.loads(pjson)

#with open('listiniApi.json') as data_file:  data = json.loads(data_file.read())
df = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['edges']))
pg = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['pageInfo']))
# at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))

NextPage = pg['hasNextPage'][0]
cursor = pg['endCursor'][0]

while NextPage:
    list = {"query": '''
    { lists(first: 100, after:"''' + cursor + '''")
    {edges {
          node {
           ...List
    } } 
    pageInfo {
    hasNextPage
    endCursor
    }
    }} 
    fragment List on List {
      sku 
      vendorCode
      availableMeasure
      customMeasurePrice {
        ...Price
      }
      customAvailableMeasuresFrom
      customAvailableMeasuresTo
      listPrice {
        ...Price
      }
      viewOnlyListPrice
      isListActive
      groups {
        ...Group
      }
    }

    fragment Price on Money {
      amount
    }

    fragment Group on ChannelList {
      group
      isChannelActive
      price {
        ...Price
      }
      priceInclTax {
        ...Price
      }
      discount {
        ...Price
      }
      discountPercentage
      discountFromDate
      discountToDate
      temporaryDiscountPercentage
    }
     '''}
    results_prod = requests.post(url=api_url, json=list)
    pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
    data = json.loads(pjson)

    # with open('listiniApi.json') as data_file:  data = json.loads(data_file.read())
    pdf = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['edges']))
    fpg = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['pageInfo']))
    # at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))
    NextPage = fpg['hasNextPage'][0]
    cursor = fpg['endCursor'][0]
    df = df.append(pdf, ignore_index=True, sort=False)
time = datetime.datetime.now()
def parse(string):
    dt = datetime.datetime.strptime(string[0:19],'%Y-%m-%dT%H:%M:%S')
    if string[19] == "+":
        dt -= datetime.timedelta(hours=int(string[20:21]),
                                 minutes=int(string[23:24]))
    elif string[19]=='-':
        dt += datetime.timedelta(hours=int(string[20:21]),
                                 minutes=int(string[23:24]))
    return dt.isoformat()
dfn = pd.DataFrame()

# Controllo listino attivo
df = df.drop(df[df['node.isListActive'].map(lambda x: x is False)].index)
# Formattazione campi BindCommerce
dfn['Code'] = df['node.sku']
dfn['Vendor'] = df['node.vendorCode']
dfn['Prices'] = df['node.groups']
for key, value in df['node.groups'].iteritems():
    for i in range(len(df['node.groups'][key])):
        if df['node.groups'][key][i]['isChannelActive'] is True:
             if (df['node.groups'][key][i]['group'] is not None and df['node.groups'][key][i]['group'] is not 'null') and (df['node.groups'][key][i]['price'] is not None and df['node.groups'][key][i]['price'] is not 'null'): # Verifica la presenza di sconti temporanei
                dfn['Prices'][key][i]['GrossPrice'] = df['node.groups'][key][i]['priceInclTax']['amount']
                dfn['Prices'][key][i]['NetPrice'] = ''
                dfn['Prices'][key][i]['NetPrice'] = df['node.groups'][key][i]['price']['amount']
                dfn['Prices'][key][i].pop('price')
                dfn['Prices'][key][i]['ListName'] = df['node.vendorCode'][key]+'_'+df['node.groups'][key][i]['group'] # nome del listino da Vendor e Canale
                dfn['Prices'][key][i].pop('group')
                if df['node.groups'][key][i]['discountFromDate'] is not None and df['node.groups'][key][i]['discountFromDate'] is not 'null':
                    if (time.isoformat() >= (parse(df['node.groups'][key][i]['discountFromDate']))) and (time.isoformat() <= parse((df['node.groups'][key][i]['discountToDate']))):
                        t = df['node.groups'][key][i]['priceInclTax']['amount']
                        s = df['node.groups'][key][i]['temporaryDiscountPercentage']
                        dfn['Prices'][key][i]['GrossPrice'] = t - (t * s) / 100
                       # print(t)
                       # print(s)
                        #print(dfn['Prices'][key][i]['GrossPrice'])
                    else:
                        dfn['Prices'][key][i]['GrossPrice'] = df['node.groups'][key][i]['priceInclTax']['amount']
             else:
                 dfn['Prices'][key][i].pop('group')
                 dfn['Prices'][key][i].pop('price')
             dfn['Prices'][key][i].pop('priceInclTax')
             dfn['Prices'][key][i].pop('isChannelActive')
             dfn['Prices'][key][i].pop('discount')
             dfn['Prices'][key][i].pop('discountPercentage')
             dfn['Prices'][key][i].pop('discountFromDate')
             dfn['Prices'][key][i].pop('discountToDate')
             dfn['Prices'][key][i].pop('temporaryDiscountPercentage')
        else:
            #dfn['Prices'][key][i] = ''
            dfn['Prices'][key][i].pop('group')
            dfn['Prices'][key][i].pop('isChannelActive')
            #dfn['Prices'][key][i].pop('isRemarketing')
            dfn['Prices'][key][i].pop('price')
            dfn['Prices'][key][i].pop('priceInclTax')
            dfn['Prices'][key][i].pop('discount')
            dfn['Prices'][key][i].pop('discountPercentage')
            dfn['Prices'][key][i].pop('discountFromDate')
            dfn['Prices'][key][i].pop('discountToDate')
            dfn['Prices'][key][i].pop('temporaryDiscountPercentage')


#Eliminare righe di listini con disponibilita nulle (in base a code e vendorcode)
dfn['QtNull'] = False
for key, value in dfn['Code'].iteritems():
    for k, v in dcode['Code'].iteritems():
        if v == value and dcode['VendorCode'][k] == dfn['Vendor'][key]:
            dfn.loc[key, 'QtNull'] = True
dfn = dfn.drop(dfn[dfn['QtNull'].map(lambda x: x is True)].index)

# Funzione per unire i listini dello stesso prodotto
def cloning(list):
    c = []
    for k, v in list.iteritems():
        c.extend(v)
    return c
dfn = dfn.sort_values(by=['Code', 'Vendor']) # Ordinamento per Codice e per Vendor
dfn = dfn.groupby(['Code'])['Prices'].agg({'Prices': lambda x: cloning(x)})  #Aggregazione dei listini per codice prodottto
dfn = dfn.reset_index(['Code'])
print(dfn)
#Elimina listini disattivi o vuoti
for key, value in dfn['Prices'].iteritems():
    while {} in value:
        value.remove({})

#Unione dei listini e stock per codice prodotto
dft = pd.merge(dfn, dfs, on='Code').fillna('')
dftp = pd.merge(dfnp, dft, on='Code').fillna('')

#Convesione nuovo file json e serializzazione
dfj = dftp.to_json(None, orient='records')
ppjson = json.loads(dfj, object_pairs_hook=collections.OrderedDict)


# Creazione file xml
#debug generazione xml
#set_debug(debug=True, filename='erroriXML.log')
prod = lambda x: x[:-1]
xml = dicttoxml(ppjson, custom_root='Products', attr_type=False, item_func=prod)
#pprint(xml)
dom = parseString(xml)
print(dom.toprettyxml())
output = open("ProductFull.xml", "w")
output.write(dom.toprettyxml(encoding="utf-8"))


#Aggiunta delle specifiche sul feed full o incremental
f = open("ProductFull.xml", "r")
contents = f.readlines()
f.close()
contents.insert(1, '<bindCommerceProducts Mode="full">'+'\n')
f = open("ProductFull.xml", "w")
contents = "".join(contents)
f.write(contents)
f.close()