import json
from itertools import chain

from pandas.io.json import json_normalize
import ijson
import pandas as pd
from pprint import pprint
from xml.dom.minidom import parseString
import requests
from dicttoxml import dicttoxml
import datetime
import collections
from dateutil import parser
# Connessione API e raccolta dati
api_url = "https://admin.oiritaly.it/api"
headers = {'Authorization': 'api-key 8ff5b7baa10cdfbaf4d973b674363b6ef83561d5'}

list = {"query": '''
{ lists(first: 100)
{edges {
      node {
       ...List
} } 
pageInfo {
hasNextPage
endCursor
}
}} 
fragment List on List {
  sku 
  vendorCode
  availableMeasure
  customMeasurePrice {
    ...Price
  }
  customAvailableMeasuresFrom
  customAvailableMeasuresTo
  listPrice {
    ...Price
  }
  viewOnlyListPrice
  isListActive
  groups {
    ...Group
  }
}

fragment Price on Money {
  amount
}

fragment Group on ChannelList {
  group
  isChannelActive
  price {
    ...Price
  }
  priceInclTax {
    ...Price
  }
  discount {
    ...Price
  }
  discountPercentage
  discountFromDate
  discountToDate
  temporaryDiscountPercentage
}
'''}

results_prod = requests.post(url=api_url, json=list)
pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
data = json.loads(pjson)

#with open('listiniApi.json') as data_file:  data = json.loads(data_file.read())
df = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['edges']))
pg = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['pageInfo']))
# at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))

NextPage = pg['hasNextPage'][0]
cursor = pg['endCursor'][0]

while NextPage:
    list = {"query": '''
    { lists(first: 100, after:"''' + cursor + '''")
    {edges {
          node {
           ...List
    } } 
    pageInfo {
    hasNextPage
    endCursor
    }
    }} 
    fragment List on List {
      sku 
      vendorCode
      availableMeasure
      customMeasurePrice {
        ...Price
      }
      customAvailableMeasuresFrom
      customAvailableMeasuresTo
      listPrice {
        ...Price
      }
      viewOnlyListPrice
      isListActive
      groups {
        ...Group
      }
    }

    fragment Price on Money {
      amount
    }

    fragment Group on ChannelList {
      group
      isChannelActive
      price {
        ...Price
      }
      priceInclTax {
        ...Price
      }
      discount {
        ...Price
      }
      discountPercentage
      discountFromDate
      discountToDate
      temporaryDiscountPercentage
    }
     '''}
    results_prod = requests.post(url=api_url, json=list)
    pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
    data = json.loads(pjson)

    # with open('listiniApi.json') as data_file:  data = json.loads(data_file.read())
    pdf = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['edges']))
    fpg = pd.DataFrame.from_dict(json_normalize(data['data']['lists']['pageInfo']))
    # at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))
    NextPage = fpg['hasNextPage'][0]
    cursor = fpg['endCursor'][0]
    df = df.append(pdf, ignore_index=True, sort=False)
time = datetime.datetime.now()
def parse(string):
    dt = datetime.datetime.strptime(string[0:19],'%Y-%m-%dT%H:%M:%S')
    if string[19] == "+":
        dt -= datetime.timedelta(hours=int(string[20:21]),
                                 minutes=int(string[23:24]))
    elif string[19]=='-':
        dt += datetime.timedelta(hours=int(string[20:21]),
                                 minutes=int(string[23:24]))
    return dt.isoformat()
dfn = pd.DataFrame()

# Controllo listino attivo
df = df.drop(df[df['node.isListActive'].map(lambda x: x is False)].index)
# Formattazione campi BindCommerce
dfn['Code'] = df['node.sku']
dfn['Vendor'] = df['node.vendorCode']
dfn['Prices'] = df['node.groups']

for key, value in df['node.groups'].iteritems():
    for i in range(len(df['node.groups'][key])):
        if df['node.groups'][key][i]['isChannelActive'] is True:
             if (df['node.groups'][key][i]['group'] is not None and df['node.groups'][key][i]['group'] is not 'null') and (df['node.groups'][key][i]['price'] is not None and df['node.groups'][key][i]['price'] is not 'null'): # Verifica la presenza di sconti temporanei
                dfn['Prices'][key][i]['GrossPrice'] = df['node.groups'][key][i]['priceInclTax']['amount']
                dfn['Prices'][key][i]['NetPrice'] = ''
                dfn['Prices'][key][i]['NetPrice'] = df['node.groups'][key][i]['price']['amount']
                dfn['Prices'][key][i].pop('price')
                dfn['Prices'][key][i]['ListName'] = df['node.vendorCode'][key]+'_'+df['node.groups'][key][i]['group'] # nome del listino da Vendor e Canale
                dfn['Prices'][key][i].pop('group')
                if df['node.groups'][key][i]['discountFromDate'] is not None and df['node.groups'][key][i]['discountFromDate'] is not 'null':
                    if (time.isoformat() >= (parse(df['node.groups'][key][i]['discountFromDate']))) and (time.isoformat() <= parse((df['node.groups'][key][i]['discountToDate']))):
                        t = df['node.groups'][key][i]['priceInclTax']['amount']
                        s = df['node.groups'][key][i]['temporaryDiscountPercentage']
                        dfn['Prices'][key][i]['GrossPrice'] = t - (t * s) / 100
                       # print(t)
                       # print(s)
                        #print(dfn['Prices'][key][i]['GrossPrice'])
                    else:
                        dfn['Prices'][key][i]['GrossPrice'] = df['node.groups'][key][i]['priceInclTax']['amount']
             else:
                 dfn['Prices'][key][i].pop('group')
                 dfn['Prices'][key][i].pop('price')
             dfn['Prices'][key][i].pop('priceInclTax')
             dfn['Prices'][key][i].pop('isChannelActive')
             dfn['Prices'][key][i].pop('discount')
             dfn['Prices'][key][i].pop('discountPercentage')
             dfn['Prices'][key][i].pop('discountFromDate')
             dfn['Prices'][key][i].pop('discountToDate')
             dfn['Prices'][key][i].pop('temporaryDiscountPercentage')
        else:
            #dfn['Prices'][key][i] = ''
            dfn['Prices'][key][i].pop('group')
            dfn['Prices'][key][i].pop('isChannelActive')
            #dfn['Prices'][key][i].pop('isRemarketing')
            dfn['Prices'][key][i].pop('price')
            dfn['Prices'][key][i].pop('priceInclTax')
            dfn['Prices'][key][i].pop('discount')
            dfn['Prices'][key][i].pop('discountPercentage')
            dfn['Prices'][key][i].pop('discountFromDate')
            dfn['Prices'][key][i].pop('discountToDate')
            dfn['Prices'][key][i].pop('temporaryDiscountPercentage')

def cloning(list): # Funzione per unire i listini dello stesso prodotto
    c = []
    for k, v in list.iteritems():
        c.extend(v)
    return c
dfn = dfn.sort_values(by=['Code', 'Vendor']) # Ordinamento per Codice e per Vendor
dfn = dfn.groupby(['Code'])['Prices'].agg({'Prices': lambda x: cloning(x)}) #Aggregazione dei listini per codice prodottto
dfn = dfn.reset_index(['Code'])

#Elimina listini disattivi o vuoti
for key, value in dfn['Prices'].iteritems():
    while {} in value:
        value.remove({})

#Convesione nuovo file json e serializzazione
dfj = dfn.to_json(None, orient='records')
ppjson = json.loads(dfj, object_pairs_hook=collections.OrderedDict)
#print(ppjson)

# creazione file xml

prod = lambda x: x[:-1]
xml = dicttoxml(ppjson, custom_root='Products', attr_type=False, item_func=prod)
dom = parseString(xml)
print(dom.toprettyxml())
output = open("listini.xml", "w")
output.write(dom.toprettyxml(encoding="utf-8"))
