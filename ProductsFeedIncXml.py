import json
from pandas.io.json import json_normalize
import ijson
import pandas as pd
from pprint import pprint
from xml.dom.minidom import parseString
import requests
from dicttoxml import dicttoxml
import collections
import datetime
# Connessione API e raccolta dati
api_url = "https://admin.oiritaly.it/api"
headers = {'Authorization': 'api-key 8ff5b7baa10cdfbaf4d973b674363b6ef83561d5'}
#Rilevazione della data e ora correnti e impostazione intervallo di tempo per rilevare modifiche
daytimenow = datetime.datetime.now().replace(second=0, microsecond=0,)
delta = -10
daytimearly = datetime.datetime.now().replace(second=0, microsecond=0,)+datetime.timedelta(minutes=delta)

products = {"query": '''
{products (first:100 updatedFrom:"'''+str(daytimearly)+'''" updatedTo:"'''+str(daytimenow)+'''") 
{edges 
{node 
{sku 
ean 
name 
productType 
category 
brand 
attributes
{name 
value 
}
descriptions {
    language
    description
  }
}}
pageInfo {
hasNextPage
endCursor
}
}
}
'''}

results_prod = requests.post(url=api_url, json=products)
pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
data = json.loads(pjson)

# with open('prodotti.json') as data_file:    data = json.loads(data_file.read())
df = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges']))
pg = pd.DataFrame.from_dict(json_normalize(data['data']['products']['pageInfo']))
# at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))
if df.isnull:
    print('Non vi sono aggiornamenti nei prodotti negli ultimi '+str(-delta)+' minuti')
else:
    NextPage = pg['hasNextPage'][0]
    cursor = pg['endCursor'][0]

    while NextPage:
        products = {"query": '''
        {products (first:100 updatedFrom:"'''+str(daytimearly)+'''" updatedTo:"'''+str(daytimenow)+'''", after:"''' + cursor + '''") 
        {edges 
        {node 
        {sku 
        ean 
        name 
        productType 
        category 
        brand 
        attributes
        {name 
        value 
        }
        descriptions {
            language
            description
          }
        }}
        pageInfo {
        hasNextPage
        endCursor
        }
        }
        }
        '''}
        results_prod = requests.post(url=api_url, json=products)
        pjson = json.dumps(results_prod.json(), indent=4, separators=(',', ': '))
        data = json.loads(pjson)
        #with open('prodotti.json') as data_file:    data = json.loads(data_file.read())
        pdf = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges']))
        fpg = pd.DataFrame.from_dict(json_normalize(data['data']['products']['pageInfo']))
        #at = pd.DataFrame.from_dict(json_normalize(data['data']['products']['edges'], record_path=['node', 'attributes'], meta=['node']))
        #concatenare dataframe pandas
        NextPage = fpg['hasNextPage'][0]
        cursor = fpg['endCursor'][0]
        df = df.append(pdf, ignore_index=True, sort=None)

    language = 'Italian'
    dfn = pd.DataFrame()
    dfn['Code'] = df['node.sku']
    dfn['Barcode'] = df['node.ean']
    dfn['BarcodeKind'] = 'EAN'
    dfn['Title'] = df['node.name']
    #for i in df['node.descriptions'].iteritems():
    dfn['ShortDescription'] = ''      #da comporre con lista di attributi da definire con Cristiano
    dfn['Categories'] = df['node.productType']
    #Le descrizioni in lingua sono disattivate
    #dfn['Alternatives']=df['node.descriptions']
    #for key, value in df['node.descriptions'].iteritems():
    #  for i in range(len(df['node.descriptions'][key])):
    ## #if df['node.attributes'][key][i]['name'] is set:
    #    dfn['Alternatives'][key][i]['Language'] = df['node.descriptions'][key][i]['language']
    #   dfn['Alternatives'][key][i].pop('language')
    #  dfn['Alternatives'][key][i]['DescriptionHtml'] = df['node.descriptions'][key][i]['description']
    #   dfn['Alternatives'][key][i].pop('description')

    dfn['Manufacturer'] = df['node.brand']
    dfn['Qty'] = ''
    dfn['Pictures'] = ''
    dfn['Attributes'] = df['node.attributes']

    for key, value in df['node.attributes'].iteritems():
        for i, v in enumerate(df['node.attributes'][key]):
            if df['node.attributes'][key][i]['value'] != ' ' and df['node.attributes'][key][i]['value'] != '' and df['node.attributes'][key][i]['value'] != '0' and df['node.attributes'][key][i]['value'] != '- non applicabile':
                dfn['Attributes'][key][i]['Name'] = df['node.attributes'][key][i]['name']
                dfn['Attributes'][key][i]['Value'] = df['node.attributes'][key][i]['value']
            dfn['Attributes'][key][i].pop('name')
            dfn['Attributes'][key][i].pop('value')


    #Eliminazione degli attributi vuoti
    for key, value in dfn['Attributes'].iteritems():
        while {} in value:
            value.remove({})

    def shortdescription(cat, attribList): #Funzione per produrre la descrizione breve
        for key, value in dfn['Attributes'].iteritems():
            for k, v in enumerate(value):
                if dfn['Categories'][key] == cat and dfn['Attributes'][key][k]['Name'] in attribList:  # Definire quali attributi inviare nella descrizione
                    dfn['ShortDescription'][key] = dfn['ShortDescription'][key] + dfn['Attributes'][key][k]['Value'] + ' '
    # Creo descrizione breve con il Titolo
    for key, value in dfn['Attributes'].iteritems():
        for k, v in enumerate(value):
            if dfn['Attributes'][key][k]['Name'] == 'Titolo':  # Definire quali attributi inviare nella descrizione
                dfn['ShortDescription'][key] = dfn['ShortDescription'][key] + dfn['Attributes'][key][k]['Value'] + ' '
    # Creo descrizione breve ottica con lista attributi
    AttribDesc = ['Genere utente', 'Forma occhiale', 'Materiale occhiali', 'Colore montatura']
    shortdescription('Occhiali e montature', AttribDesc)
    shortdescription('Lenti a contatto', AttribDesc)
    # Creo descrizione breve gioielli con lista attributi
    AttribDescGioi = ['Genere', 'Materiale', 'Colore']
    shortdescription('Gioielli', AttribDescGioi)
    # Creo descrizione breve orologi con lista attributi
    AttribDescWatch = ['Genere', 'Alimentazione movimento', 'Materiale', 'Display', 'Colore']
    shortdescription('Orologi', AttribDescWatch)

    # Ordinamento per Codice
    dfn = dfn.sort_values(by='Code')

    #Convesione nuovo file json e serializzazione

    dfj = dfn.to_json(None, orient='records')
    #print(dfj)
    ppjson = json.loads(dfj, object_pairs_hook=collections.OrderedDict)
    #print(ppjson)
    #output = open("Nprodotti.json", "w")
    #output.write(dfj)


    # creazione file xml

    prod = lambda x: x[:-1]
    xml = dicttoxml(ppjson, custom_root='Products', attr_type=False, item_func=prod)
    #pprint(xml)
    dom = parseString(xml)
    print(dom.toprettyxml())
    output = open("ProductsINC.xml", "w")
    output.write(dom.toprettyxml(encoding="utf-8"))
    #debug generazione xml
    #dicttoxml.set_debug(filename='DebugFeedProdotti.log')

    #Aggiunta delle specifiche sul feed full o incremental
    f = open("ProductsINC.xml", "r")
    contents = f.readlines()
    f.close()
    contents.insert(1, '<bindCommerceProducts Mode="incremental">'+'\n')
    f = open("ProductsINC.xml", "w")
    contents = "".join(contents)
    f.write(contents)
    f.close()